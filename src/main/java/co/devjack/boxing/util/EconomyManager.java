package co.devjack.boxing.util;

import co.devjack.boxing.BoxingMain;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.entity.Player;

public class EconomyManager {
    public static void payoutWinner(Player p) {
        if (BoxingMain.getInstance().econ != null) {
            EconomyResponse r = BoxingMain.getInstance().econ.depositPlayer(p.getName(), BoxingMain.getInstance().getConfig().getInt("WinnersReward"));
            if (r.transactionSuccess()) {
                p.sendMessage(String.format(BoxingMain.getInstance().prefix + "You have been rewarded %s for winning the Boxing match", BoxingMain.econ.format(r.amount)));
            } else {
                p.sendMessage(String.format(BoxingMain.getInstance().prefix + "An error occured: %s", r.errorMessage));
            }
        }
    }

    public static void payoutLoser(Player p) {
        if (BoxingMain.getInstance().econ != null) {
            EconomyResponse r = BoxingMain.econ.depositPlayer(p.getName(), BoxingMain.getInstance().getConfig().getInt("LosersReward"));
            if (r.transactionSuccess()) {
                p.sendMessage(String.format(BoxingMain.getInstance().prefix + "You have been rewarded %s for participation the Boxing match", BoxingMain.econ.format(r.amount)));
            } else {
                p.sendMessage(String.format(BoxingMain.getInstance().prefix + "An error occured: %s", r.errorMessage));
            }
        }
    }
}
