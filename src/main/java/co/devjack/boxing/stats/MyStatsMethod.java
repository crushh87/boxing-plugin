package co.devjack.boxing.stats;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

public class MyStatsMethod {
    private static BoxingMain pl;

    public MyStatsMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void showMyStats(Player p) {
        if (pl.StatsConfig.contains("Players." + p.getName())) {
            Double wins = pl.StatsConfig.getDouble("Players." + p.getName() + ".Wins");
            Double losses = pl.StatsConfig.getDouble("Players." + p.getName() + ".Losses");
            Double KD = wins / losses;

            p.sendMessage("�6---> �3" + p.getDisplayName() + "�3's stats �6<---");
            p.sendMessage("�6Total Wins: �3" + wins);
            p.sendMessage("�6Total Losses: �3" + losses);
            p.sendMessage("�6Win/Loss Ratio: �3" + KD);
        } else {
            p.sendMessage(pl.prefix + "You have never played any Boxing matches!");
        }
    }
}
