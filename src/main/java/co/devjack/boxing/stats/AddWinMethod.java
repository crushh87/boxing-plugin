package co.devjack.boxing.stats;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

public class AddWinMethod {
    private static BoxingMain pl;

    public AddWinMethod(BoxingMain instance) {
        pl = instance;
    }


    @SuppressWarnings("static-access")
    public static void addWin(Player p) {
        if (!pl.StatsConfig.contains("Players." + p.getName())) {
            pl.StatsConfig.set("Players." + p.getName() + ".Wins", pl.StatsConfig.getInt("Players." + p.getName() + ".Wins") + 1);
        } else {
            pl.StatsConfig.set("Players." + p.getName() + ".Wins", 1);
        }
    }
}
