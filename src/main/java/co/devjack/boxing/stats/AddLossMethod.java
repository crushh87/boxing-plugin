package co.devjack.boxing.stats;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

public class AddLossMethod {
    private static BoxingMain pl;

    public AddLossMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void addLoss(Player p) {
        if (pl.StatsConfig.contains("Players." + p.getName())) {
            pl.StatsConfig.set("Players." + p.getName() + ".Losses", pl.StatsConfig.getInt("Players." + p.getName() + ".Losses") + 1);
        } else {
            pl.StatsConfig.set("Players." + p.getName() + ".Losses", 1);
        }
    }
}
