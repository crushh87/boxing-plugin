package co.devjack.boxing.stats;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TopWLR {
    private static BoxingMain pl;

    public TopWLR(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void showTopWLR(Player p) {
        final HashMap<String, Double> statsWLR = new HashMap<String, Double>();
        int i = 0;

        for (String player : pl.StatsConfig.getConfigurationSection("Players").getKeys(false)) {
            int wins = pl.StatsConfig.getInt("Players." + player + ".Wins");
            int losses = pl.StatsConfig.getInt("Players." + player + ".Losses");
            Double WLR = (double) (wins / losses);
            statsWLR.put(player, WLR);
        }

        List<Double> intList = new ArrayList<Double>();
        for (Entry<String, Double> e : statsWLR.entrySet()) {
            Double number = e.getValue();
            intList.add(number);
        }

        Collections.sort(intList, Collections.reverseOrder());
        p.sendMessage("�6---> �3Top 10 Boxing Win/Loss Ratios �6<---");
        for (Double num : intList) {
            for (Entry<String, Double> e : statsWLR.entrySet()) {
                String pName = e.getKey();
                Double number = e.getValue();
                if (i > 10) {
                    break;
                }
                if (number.equals(num)) {
                    p.sendMessage("�6" + pName + "�6: �3" + num);
                    i++;
                }
            }
        }
    }
}
