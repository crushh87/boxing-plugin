package co.devjack.boxing.stats;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TopWinsMethod {
    private static BoxingMain pl;

    public TopWinsMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void showTopWins(Player p) {
        final HashMap<String, Integer> statsWins = new HashMap<String, Integer>();
        int i = 0;

        for (String player : pl.StatsConfig.getConfigurationSection("Players").getKeys(false)) {
            int wins = pl.StatsConfig.getInt("Players." + player + ".Wins");
            statsWins.put(player, wins);
        }

        List<Integer> intList = new ArrayList<Integer>();
        for (Entry<String, Integer> e : statsWins.entrySet()) {
            String pName = e.getKey();
            Integer number = e.getValue();
            intList.add(number);
        }

        Collections.sort(intList, Collections.reverseOrder());
        p.sendMessage("�6---> �3Top 10 Boxing Winners �6<---");
        for (int num : intList) {
            for (Entry<String, Integer> e : statsWins.entrySet()) {
                String pName = e.getKey();
                Integer number = e.getValue();

                if (number.equals(num)) {
                    p.sendMessage("�6" + pName + "�6: �3" + num);
                    i++;
                }
                if (i > 9) {
                    break;
                }
            }
        }
    }
}
