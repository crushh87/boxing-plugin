package co.devjack.boxing.signs;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.system.JoinGameMethod;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignInteract implements Listener {
    private BoxingMain pl;

    public SignInteract(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getState() instanceof Sign) {
                Sign s = (Sign) e.getClickedBlock().getState();
                if (s.getLine(0).equalsIgnoreCase("[Boxing]")) {
                    if (s.getLine(1).equalsIgnoreCase("Join")) {
                        String arena = s.getLine(2).toLowerCase();
                        if (pl.DataConfig.contains("Arenas." + arena)) {
                            JoinGameMethod.joinGame(e.getPlayer(), arena);
                        }
                    }
                    if (s.getLine(1).equalsIgnoreCase("Leave")) {
                        String arena1 = s.getLine(2).toLowerCase();
                        if (pl.DataConfig.contains("Arenas." + arena1)) {
                            e.getPlayer().performCommand("bx leave");
                        }
                    }
                }
            }
        }
    }
}
