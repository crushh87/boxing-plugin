package co.devjack.boxing.signs;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignChange implements Listener {
    private BoxingMain pl;

    public SignChange(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onSignChange(SignChangeEvent ev) {
        Player p = ev.getPlayer();
        String[] signline = ev.getLines();
        p.sendMessage("SignChange");
        if (p.hasPermission("bx.admin")) {
            if (signline[0].equalsIgnoreCase("[Boxing]")) {
                if (signline[1].equalsIgnoreCase("Join")) {
                    for (String arenas : pl.DataConfig.getConfigurationSection("Arenas").getKeys(false)) {
                        if (signline[2].equalsIgnoreCase(arenas)) {
                            p.sendMessage(pl.prefix + "Join sign for arena �3" + arenas + " �6created!");
                        }
                    }
                }
                if (signline[1].equalsIgnoreCase("Leave")) {
                    for (String arenas : pl.DataConfig.getConfigurationSection("Arenas").getKeys(false)) {
                        if (signline[2].equalsIgnoreCase(arenas)) {
                            p.sendMessage(pl.prefix + "Leave sign for arena �3" + arenas + " �6created!");
                        }
                    }
                }
            }
        }
    }
}
