package co.devjack.boxing.eventhandlers;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.system.GetArenaMethod;
import co.devjack.boxing.system.GetPlayerByArenaMethod;
import co.devjack.boxing.system.RemovePlayerMethod;
import co.devjack.boxing.system.tpToLobbyMethod;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeave implements Listener {
    private BoxingMain pl;

    public PlayerLeave(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent ev) {
        Player p = ev.getPlayer();

        if (pl.player1.containsKey(p.getName())) {
            String arena = GetArenaMethod.getArena(p);
            Player p2 = GetPlayerByArenaMethod.getPlayerByArena(arena);
            if (pl.player2.containsValue(arena)) {
                RemovePlayerMethod.removePlayer(p);
                tpToLobbyMethod.tpToLobby(p, arena);

                RemovePlayerMethod.removePlayer(p2);
                tpToLobbyMethod.tpToLobby(p2, arena);
                p2.sendMessage(pl.prefix + "Your opponent �3" + p.getDisplayName() + " �6has left the game!");
            } else {
                RemovePlayerMethod.removePlayer(p);
                tpToLobbyMethod.tpToLobby(p, arena);
            }
        }

        if (pl.player2.containsKey(p.getName())) {
            String arena = GetArenaMethod.getArena(p);
            Player p2 = GetPlayerByArenaMethod.getPlayerByArena(arena);
            if (pl.player1.containsValue(arena)) {
                RemovePlayerMethod.removePlayer(p);
                tpToLobbyMethod.tpToLobby(p, arena);

                RemovePlayerMethod.removePlayer(p2);
                tpToLobbyMethod.tpToLobby(p2, arena);
                p2.sendMessage(pl.prefix + "Your opponent �3" + p.getDisplayName() + " �6has left the game!");
            } else {
                RemovePlayerMethod.removePlayer(p);
                tpToLobbyMethod.tpToLobby(p, arena);
            }
        }

        /*if (pl.player1.containsKey(ev.getPlayer().getName())) {
            for (Entry<String, String> e : pl.player1.entrySet()) {
                String key = e.getKey();
                String value = e.getValue();
                Player p1 = Bukkit.getServer().getPlayer(key);
                if (key.equalsIgnoreCase(p.getName())) {
                    if (pl.player2.containsValue(value)) {
                        for (Entry<String, String> e2 : pl.player2.entrySet()) {
                            String key2 = e2.getKey();
                            String value2 = e2.getValue();
                            Player p2 = Bukkit.getServer().getPlayer(key2);
                            if (value.equals(value2)) {
                                RemovePlayerMethod.removePlayer(p2);
                                tpToLobbyMethod.tpToLobby(p2, value2);
                                p2.sendMessage(pl.prefix + "Your opponent has left the game!");
                            }
                        }
                    } else {
                        RemovePlayerMethod.removePlayer(p);
                        tpToLobbyMethod.tpToLobby(p, value);
                    }
                }
            }
        }

        if (pl.player2.containsKey(ev.getPlayer().getName())) {
            for (Entry<String, String> e : pl.player2.entrySet()) {
                String key = e.getKey();
                String value = e.getValue();
                Player p1 = Bukkit.getServer().getPlayer(key);
                if (key.equalsIgnoreCase(p.getName())) {
                    if (pl.player1.containsValue(value)) {
                        RemovePlayerMethod.removePlayer(p);
                        tpToLobbyMethod.tpToLobby(p, value);
                        for (Entry<String, String> e2 : pl.player1.entrySet()) {
                            String key2 = e2.getKey();
                            String value2 = e2.getValue();
                            Player p2 = Bukkit.getServer().getPlayer(key2);
                            if (value.equals(value2)) {
                                RemovePlayerMethod.removePlayer(p2);
                                tpToLobbyMethod.tpToLobby(p2, value);
                                p2.sendMessage(pl.prefix + "Your opponent has left the game!");
                            }
                        }
                    } else {
                        RemovePlayerMethod.removePlayer(p);
                        tpToLobbyMethod.tpToLobby(p, value);
                    }
                }
            }
        }*/
    }
}
