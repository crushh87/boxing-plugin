package co.devjack.boxing.eventhandlers;

import co.devjack.boxing.BoxingMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class Building implements Listener {
    private BoxingMain pl;

    public Building(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onBlockBreak(BlockBreakEvent ev) {
        if (pl.player1.containsKey(ev.getPlayer().getName()) || pl.player2.containsKey(ev.getPlayer().getName())) {
            if (!ev.getPlayer().hasPermission("bx.admin")) {
                ev.setCancelled(true);
                ev.getPlayer().sendMessage(pl.prefix + "You can't break blocks in the arena");
            }
        }
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent ev) {
        if (pl.player1.containsKey(ev.getPlayer().getName()) || pl.player2.containsKey(ev.getPlayer().getName())) {
            if (!ev.getPlayer().hasPermission("bx.admin")) {
                ev.setCancelled(true);
                ev.getPlayer().sendMessage(pl.prefix + "You can't place blocks in the arena");
            }
        }
    }
}
