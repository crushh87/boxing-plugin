package co.devjack.boxing.eventhandlers;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class TeleportFix implements Listener {
    private BoxingMain pl;

    public TeleportFix(BoxingMain instance) {
        pl = instance;
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.canSee(event.getPlayer())) {
                p.showPlayer(event.getPlayer());
            }
        }
    }
}
