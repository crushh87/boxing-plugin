package co.devjack.boxing.eventhandlers;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.stats.AddLossMethod;
import co.devjack.boxing.stats.AddWinMethod;
import co.devjack.boxing.system.GetArenaMethod;
import co.devjack.boxing.system.RemovePlayerMethod;
import co.devjack.boxing.system.tpToLobbyMethod;
import net.minecraft.server.v1_6_R2.Packet205ClientCommand;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_6_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import static co.devjack.boxing.util.EconomyManager.payoutLoser;
import static co.devjack.boxing.util.EconomyManager.payoutWinner;

public class PlayerDeath implements Listener {
    private BoxingMain pl;

    public PlayerDeath(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent ev) {
        final Player p = (Player) ev.getEntity();
        Player killer = p.getKiller();

        if (pl.player1.containsKey(p.getName())) {
            //We know Player 1 lost the match, and player2 won it.
            final String arena = GetArenaMethod.getArena(p);
            tpToLobbyMethod.tpToLobby(killer, arena);

            Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
                @Override
                public void run() {
                    Packet205ClientCommand packet = new Packet205ClientCommand();
                    packet.a = 1;
                    ((CraftPlayer) p).getHandle().playerConnection.a(packet);
                    tpToLobbyMethod.tpToLobby(p, arena);
                }
            }, 5L);

            killer.sendMessage(pl.prefix + "You have won the boxing match against �3" + p.getName());
            p.sendMessage(pl.prefix + "You have lost the boxing match against �3" + killer.getName());
            Bukkit.broadcastMessage(pl.prefix + "Player �3" + killer.getName() + " �6has beat �3" + p.getName() + " �6in a boxing match on arena �3" + arena);

            float health = 20;
            killer.setHealth(health);

            RemovePlayerMethod.removePlayer(killer);
            RemovePlayerMethod.removePlayer(p);
            payoutWinner(killer);
            payoutLoser(p);
            AddLossMethod.addLoss(p);
            AddWinMethod.addWin(killer);
        }

        if (pl.player2.containsKey(p.getName())) {
            final String arena = GetArenaMethod.getArena(p);
            tpToLobbyMethod.tpToLobby(killer, arena);

            Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
                @Override
                public void run() {
                    Packet205ClientCommand packet = new Packet205ClientCommand();
                    packet.a = 1;
                    ((CraftPlayer) p).getHandle().playerConnection.a(packet);
                    tpToLobbyMethod.tpToLobby(p, arena);
                }
            }, 5L);

            killer.sendMessage(pl.prefix + "You have won the boxing match against �3" + p.getName());
            p.sendMessage(pl.prefix + "You have lost the boxing match against �3" + killer.getName());
            Bukkit.broadcastMessage(pl.prefix + "Player �3" + killer.getName() + " �6has beat �3" + p.getName() + " �6in a boxing match on arena �3" + arena);

            float health = 20;
            killer.setHealth(health);
            RemovePlayerMethod.removePlayer(killer);
            RemovePlayerMethod.removePlayer(p);
            payoutWinner(killer);
            payoutLoser(p);
            AddLossMethod.addLoss(p);
            AddWinMethod.addWin(killer);
        }
    }
}
