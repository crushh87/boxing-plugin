// I warn you now....
// I am NOT the best coder of all times.
// You may find some bugs...
// If you find a bug... Tell me...

// Use as you wish, just don't claim as your own :D
// I worked hard on this, don't steal my work!

package co.devjack.boxing;

import co.devjack.boxing.eventhandlers.Building;
import co.devjack.boxing.eventhandlers.PlayerDeath;
import co.devjack.boxing.eventhandlers.PlayerLeave;
import co.devjack.boxing.eventhandlers.TeleportFix;
import co.devjack.boxing.signs.SignChange;
import co.devjack.boxing.signs.SignInteract;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class BoxingMain extends JavaPlugin {
    public static BoxingMain instance;

    public static HashMap<String, String> player1 = new HashMap<String, String>();
    public static HashMap<String, String> player2 = new HashMap<String, String>();

    public static HashMap<String, ItemStack[]> invs = new HashMap<String, ItemStack[]>();
    public static HashMap<String, ItemStack[]> armor = new HashMap<String, ItemStack[]>();
    public static HashMap<String, Float> exp = new HashMap<String, Float>();


    public static String prefix = "�6[�3Boxing�6] " + ChatColor.GOLD;
    public static String version = "2.0";
    public static String noPermission = "No Permission";

    public static File pluginFolder;
    public static File configFile;
    public static FileConfiguration DataConfig;

    public static File statsFile;
    public static FileConfiguration StatsConfig;

    //Vault
    public static Economy econ = null;

    public void onEnable() {
        instance = this;

        final FileConfiguration config = this.getConfig();
        pluginFolder = getDataFolder();
        getCommand("boxing").setExecutor(new CmdExecutor());
        loadConfiguration();
        pluginFolder = getDataFolder();
        configFile = new File(pluginFolder, "ArenaData.yml");
        DataConfig = new YamlConfiguration();

        statsFile = new File(pluginFolder, "Stats.yml");
        StatsConfig = new YamlConfiguration();

        try {
            if (!pluginFolder.exists()) {
                pluginFolder.mkdir();
            }
            if (!configFile.exists()) {
                configFile.createNewFile();
            }
            DataConfig.load(configFile);
            if (!statsFile.exists()) {
                statsFile.createNewFile();
            }
            StatsConfig.load(statsFile);
        } catch (Exception ex) {
        }

        getServer().getPluginManager().registerEvents(new PlayerDeath(this), this);
        getServer().getPluginManager().registerEvents(new PlayerLeave(this), this);
        getServer().getPluginManager().registerEvents(new TeleportFix(this), this);
        getServer().getPluginManager().registerEvents(new SignInteract(this), this);
        getServer().getPluginManager().registerEvents(new SignChange(this), this);
        getServer().getPluginManager().registerEvents(new Building(this), this);

        if (getConfig().getBoolean("UseEconomy")) {
            if (!setupEconomy()) {
                Bukkit.getConsoleSender().sendMessage(prefix + "Boxing Disabled due to no Vault dependency found!");
                Bukkit.getConsoleSender().sendMessage(prefix + "Set 'UseEconomy' to false in the config to skip Vault verification.");
                getServer().getPluginManager().disablePlugin(this);
                return;
            } else {
                Bukkit.getConsoleSender().sendMessage(prefix + "Sucessfully hooked with Vault");
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(prefix + "UseEconomy is set to false!");
            Bukkit.getConsoleSender().sendMessage(prefix + "No monetary rewards will be given!");
        }
    }

    public void onDisable() {
        try {
            DataConfig.save(configFile);
            StatsConfig.save(statsFile);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        instance = null;

        player1 = null;
        player2 = null;

        invs = null;
        armor = null;
        exp = null;

        prefix = null;
        version = null;
        noPermission = null;

        File pluginFolder = null;
        File configFile = null;
        FileConfiguration DataConfig = null;

        File statsFile = null;
        FileConfiguration StatsConfig = null;

        econ = null;
    }


    //Vault Stuff....
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    //Config stuff
    public void loadConfiguration() {
        getConfig().options().header("Boxing Config. Vault Required for 'UseEconomy: true' (DedicatedMode and BungeeServer by the sexy rtainc.");
        getConfig().addDefault("UseEconomy", false);
        getConfig().addDefault("WinnersReward", 35);
        getConfig().addDefault("LosersReward", 5);
        getConfig().addDefault("DedicatedMode", false);
        getConfig().addDefault("BungeeServerOnRoundEnd", "none");
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public static BoxingMain getInstance() {
        return instance;
    }
}
