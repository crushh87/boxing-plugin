package co.devjack.boxing;


import co.devjack.boxing.subcommands.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class CmdExecutor implements CommandExecutor {


    public boolean onCommand(CommandSender s, Command c, String str, String[] a) {
        if (a.length == 0) {
            s.sendMessage("�3[�6BX�3] �6 Boxing, made by �3crushh87.");
            s.sendMessage("�3[�6BX�3] �6 For help do �3/bx help");
            return true;
        } else if (a[0].equalsIgnoreCase("leave")) {
            new LeaveArena(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("setspawn1")) {
            new Spawn1(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("setspawn2")) {
            new Spawn2(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("help")) {
            new Help(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("list")) {
            new List(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("stats")) {
            new StatsCommand(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("join")) {
            new Join(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("createarena")) {
            new CreateArena(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("setlobby")) {
            new Lobby(s, c, a);
            return true;
        } else if (a[0].equalsIgnoreCase("delarena")) {
            new DeleteArena(s, c, a);
            return true;
        } else {
            return false;
        }
    }
}
