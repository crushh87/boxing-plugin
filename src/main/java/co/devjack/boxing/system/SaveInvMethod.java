package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SaveInvMethod {
    private static BoxingMain pl;

    public SaveInvMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void saveInv(String arena, Player p, Player p2) {
        pl.invs.put(p.getName(), p.getInventory().getContents());
        pl.armor.put(p.getName(), p.getInventory().getArmorContents());
        pl.exp.put(p.getName(), p.getExp());

        pl.invs.put(p2.getName(), p2.getInventory().getContents());
        pl.armor.put(p2.getName(), p2.getInventory().getArmorContents());
        pl.exp.put(p2.getName(), p2.getExp());

        p.getInventory().clear();
        p.getInventory().setArmorContents(new ItemStack[4]);
        p.setExp(0);

        p2.getInventory().clear();
        p2.setExp(0);
        p2.getInventory().setArmorContents(new ItemStack[4]);

        p.setHealth(20);
        p2.setHealth(20);

        p.setFoodLevel(20);
        p2.setFoodLevel(20);

        new CountDownTask(arena, p, p2).runTaskAsynchronously(BoxingMain.getInstance());
    }
}
