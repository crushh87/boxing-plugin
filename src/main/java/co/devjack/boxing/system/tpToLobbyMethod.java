package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class tpToLobbyMethod {
    private static BoxingMain pl;

    public tpToLobbyMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void tpToLobby(Player p, String map) {
        Double lobbyx = pl.DataConfig.getDouble("Arenas." + map + ".Lobby.x");
        Double lobbyy = pl.DataConfig.getDouble("Arenas." + map + ".Lobby.y");
        Double lobbyz = pl.DataConfig.getDouble("Arenas." + map + ".Lobby.z");
        float lobbypitch = pl.DataConfig.getInt("Arenas." + map + ".Lobby.pitch");
        float lobbyyaw = pl.DataConfig.getInt("Arenas." + map + ".Lobby.yaw");
        World lobbyworld = Bukkit.getServer().getWorld(pl.DataConfig.getString("Arenas." + map + ".Lobby.world"));
        Location loc = new Location(lobbyworld, lobbyx, lobbyy, lobbyz, lobbyyaw, lobbypitch);

        if (p.getLocation() != null) {
            p.teleport(loc);
        }
    }
}