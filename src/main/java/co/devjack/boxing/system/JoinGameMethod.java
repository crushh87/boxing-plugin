package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map.Entry;

public class JoinGameMethod {
    private static BoxingMain pl;

    public JoinGameMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void joinGame(Player p, String arena) {
        if (pl.player1.containsKey(p.getName()) || pl.player2.containsKey(p.getName())) {
            //We know the player is in a game.
            p.sendMessage(pl.prefix + "You are already in a game. (/bx leave)");
        } else {
            //We know the player is NOT in a game.

            if (pl.player1.containsValue(arena) && pl.player2.containsValue(arena)) {
                p.sendMessage(pl.prefix + "Arena is full.");
            } else {
                if (!pl.player1.containsValue(arena)) {
                    // We know a player is NOT in the first spot of an arena.
                    //add the player
                    pl.player1.put(p.getName(), arena);
                    p.sendMessage(pl.prefix + "You have joined the boxing match on arena �3" + arena);
                    tpToLobbyMethod.tpToLobby(p, arena);
                } else if (!pl.player2.containsValue(arena)) {
                    // We know a player is NOT in the second spot of an arena.
                    //add the player
                    pl.player2.put(p.getName(), arena);
                    p.sendMessage(pl.prefix + "You have joined the boxing match on arena �3" + arena);
                    tpToLobbyMethod.tpToLobby(p, arena);
                }
            }

            if (pl.player1.containsValue(arena) && pl.player2.containsValue(arena)) {
                for (Entry<String, String> e : pl.player1.entrySet()) {
                    String key = e.getKey();
                    String value = e.getValue();
                    Player p1 = Bukkit.getServer().getPlayer(key);
                    if (value.equalsIgnoreCase(arena)) {
                        for (Entry<String, String> e2 : pl.player2.entrySet()) {
                            String key2 = e2.getKey();
                            String value2 = e2.getValue();
                            Player p2 = Bukkit.getServer().getPlayer(key2);
                            if (value2.equalsIgnoreCase(arena)) {
                                SaveInvMethod.saveInv(value2, p1, p2);
                            }
                        }
                    }
                }
            }
        }
    }
}
