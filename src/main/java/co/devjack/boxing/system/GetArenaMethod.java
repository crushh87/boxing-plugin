package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

public class GetArenaMethod {
    private static BoxingMain pl;

    public GetArenaMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static String getArena(Player p) {
        if (pl.player1.containsKey(p.getName())) {
            return pl.player1.get(p.getName());
        } else if (pl.player2.containsKey(p.getName())) {
            return pl.player2.get(p.getName());
        } else {
            return null;
        }
    }
}
