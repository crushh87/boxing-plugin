package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map.Entry;

public class GetPlayerByArenaMethod {
    private static BoxingMain pl;

    public GetPlayerByArenaMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static Player getPlayerByArena(String arena) {
        if (pl.player1.containsValue(arena)) {
            for (Entry<String, String> e : pl.player1.entrySet()) {
                String pNames = e.getKey();
                String arena1 = e.getValue();
                Player p = Bukkit.getServer().getPlayer(pNames);
                if (arena1.equals(arena)) {
                    return p;
                }
            }
        }
        if (pl.player2.containsValue(arena)) {
            for (Entry<String, String> e : pl.player2.entrySet()) {
                String pNames = e.getKey();
                String arena1 = e.getValue();
                Player p = Bukkit.getServer().getPlayer(pNames);
                if (arena1.equals(arena)) {
                    return p;
                }
            }
        }
        return null;
    }
}
