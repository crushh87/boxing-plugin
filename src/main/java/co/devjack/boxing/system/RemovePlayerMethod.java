package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;

public class RemovePlayerMethod {
    private static BoxingMain pl;

    public RemovePlayerMethod(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public static void removePlayer(Player p) {
        String arena = GetArenaMethod.getArena(p);
        if (pl.player1.containsKey(p.getName())) {
            pl.player1.remove(p.getName());
            pl.player1.remove(arena);
        } else if (pl.player2.containsKey(p.getName())) {
            pl.player2.remove(p.getName());
            pl.player2.remove(arena);
        }

        if (pl.invs.containsKey(p.getName())) {
            p.getInventory().clear();
            p.getInventory().setContents(pl.invs.get(p.getName()));
            pl.invs.remove(p.getName());
        }

        if (pl.armor.containsKey(p.getName())) {
            p.getInventory().setArmorContents(pl.armor.get(p.getName()));
            pl.armor.remove(p.getName());
        }

        if (pl.exp.containsKey(p.getName())) {
            p.setExp(pl.exp.get(p.getName()));
            pl.exp.remove(p.getName());
        }
    }
}
