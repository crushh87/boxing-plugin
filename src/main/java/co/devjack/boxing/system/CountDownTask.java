package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class CountDownTask extends BukkitRunnable {
    private final String arena;
    private final Player p;
    private final Player p2;

    public CountDownTask(String arena, Player p, Player p2) {
        this.arena = arena;
        this.p = p;
        this.p2 = p2;
    }

    public void run() {
        try {
            p.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 5");
            p2.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 5");
            Thread.sleep(1000);
            p.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 4");
            p2.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 4");
            Thread.sleep(1000);
            p.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 3");
            p2.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 3");
            Thread.sleep(1000);
            p.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 2");
            p2.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 2");
            Thread.sleep(1000);
            p.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 1");
            p2.sendMessage(BoxingMain.getInstance().prefix + "Arena �3" + arena + " �6starting in 1");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new tpToArenaMethod(arena, p, p2).runTask(BoxingMain.getInstance());
    }
}
