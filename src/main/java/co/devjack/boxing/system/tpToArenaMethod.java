package co.devjack.boxing.system;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class tpToArenaMethod extends BukkitRunnable {
    private final String arena;
    private final Player p;
    private final Player p2;

    public tpToArenaMethod(String arena, Player p, Player p2) {
        this.arena = arena;
        this.p = p;
        this.p2 = p2;
    }

    public void run() {
        Double spawn1X = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn1.X");
        Double spawn1Y = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn1.Y");
        Double spawn1Z = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn1.Z");
        float spawn1Pitch = BoxingMain.getInstance().DataConfig.getInt("Arenas." + arena + ".Spawn1.Pitch");
        float spawn1Yaw = BoxingMain.getInstance().DataConfig.getInt("Arenas." + arena + ".Spawn1.Yaw");
        World spawn1World = Bukkit.getServer().getWorld(BoxingMain.getInstance().DataConfig.getString("Arenas." + arena + ".Spawn1.World"));
        Location player1 = new Location(spawn1World, spawn1X, spawn1Y, spawn1Z, spawn1Yaw, spawn1Pitch);

        Double spawn2X = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn2.X");
        Double spawn2Y = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn2.Y");
        Double spawn2Z = BoxingMain.getInstance().DataConfig.getDouble("Arenas." + arena + ".Spawn2.Z");
        float spawn2Pitch = BoxingMain.getInstance().DataConfig.getInt("Arenas." + arena + ".Spawn2.Pitch");
        float spawn2Yaw = BoxingMain.getInstance().DataConfig.getInt("Arenas." + arena + ".Spawn2.Yaw");
        World spawn2World = Bukkit.getServer().getWorld(BoxingMain.getInstance().DataConfig.getString("Arenas." + arena + ".Spawn2.World"));
        Location player2 = new Location(spawn2World, spawn2X, spawn2Y, spawn2Z, spawn2Yaw, spawn2Pitch);

        if (BoxingMain.getInstance().player1.containsKey(p.getName())) {
            if (p.getLocation() != null) {
                p.teleport(player1);
                p.setGameMode(GameMode.SURVIVAL);
            }
        }

        if (BoxingMain.getInstance().player2.containsKey(p2.getName())) {
            if (p.getLocation() != null) {
                p2.teleport(player2);
                p2.setGameMode(GameMode.SURVIVAL);
            }
        }
    }
}
