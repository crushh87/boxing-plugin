package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class DeleteArena {
    private BoxingMain pl;

    public DeleteArena(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public DeleteArena(CommandSender s, Command c, String[] a) {
        if (s.hasPermission("bx.admin")) {
            if (a.length == 2) {
                String map = a[1].toLowerCase();
                if (pl.DataConfig.contains("Arenas." + map)) {
                    pl.DataConfig.set("Arenas." + map, null);
                    s.sendMessage(pl.prefix + "Arena " + map + " �6deleted!");
                } else {
                    s.sendMessage(pl.prefix + "Arena does not exist");
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect Usage. /bx delarena <Name>");
            }
        } else {
            s.sendMessage(pl.prefix + pl.noPermission);
        }
    }
}
