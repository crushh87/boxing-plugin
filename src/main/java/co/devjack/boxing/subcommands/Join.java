package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.system.JoinGameMethod;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Join {
    private BoxingMain pl;

    public Join(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public Join(CommandSender s, Command c, String[] a) {
        if (s instanceof Player) {
            Player p = (Player) s;
            if (a.length == 2) {
                if (s.hasPermission("bx.player")) {
                    String map = a[1].toLowerCase();
                    if (pl.DataConfig.contains("Arenas." + map)) {
                        JoinGameMethod.joinGame(p, map);
                    } else {
                        s.sendMessage(pl.prefix + "No such arena exists");
                    }
                } else {
                    s.sendMessage(pl.prefix + pl.noPermission);
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect Usage. /bx join <Arena>");
            }
        } else {
            s.sendMessage(pl.prefix + "You have to be a player to use this command!");
        }
    }
}
