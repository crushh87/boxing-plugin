package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

public class Spawn2 {
    private BoxingMain pl;

    public Spawn2(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public Spawn2(CommandSender s, Command c, String[] a) {
        if (s instanceof Player) {
            if (a.length == 2) {
                String map = a[1].toLowerCase();
                if (s.hasPermission("bx.admin")) {
                    if (pl.DataConfig.contains("Arenas." + map)) {
                        Location loc = ((Player) s).getLocation();
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.X", loc.getX());
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.Y", loc.getY());
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.Z", loc.getZ());
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.Pitch", loc.getPitch());
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.Yaw", loc.getYaw());
                        pl.DataConfig.set("Arenas." + map + ".Spawn2.World", loc.getWorld().getName());
                        s.sendMessage(pl.prefix + "Spawn 2 for arena �3" + map + " �6saved");
                        try {
                            pl.DataConfig.save(pl.configFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        s.sendMessage(pl.prefix + "Arena does not exist");
                    }
                } else {
                    s.sendMessage(pl.prefix + pl.noPermission);
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect Usage. (/bx setspawn2 <Arena>");
            }
        } else {
            s.sendMessage(pl.prefix + "You have to be a player to use this command!");
        }
    }
}
