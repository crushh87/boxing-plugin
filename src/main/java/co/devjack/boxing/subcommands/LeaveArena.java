package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.system.GetArenaMethod;
import co.devjack.boxing.system.GetPlayerByArenaMethod;
import co.devjack.boxing.system.RemovePlayerMethod;
import co.devjack.boxing.system.tpToLobbyMethod;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveArena {
    private BoxingMain pl;

    public LeaveArena(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public LeaveArena(CommandSender s, Command c, String[] a) {
        if (s instanceof Player) {
            if (a.length == 1) {
                if (s.hasPermission("bx.player")) {
                    Player p = (Player) s;

                    String arena = GetArenaMethod.getArena(p);
                    Player p2 = GetPlayerByArenaMethod.getPlayerByArena(arena);

                    if ((pl.player1.containsKey(p.getName()) && pl.player2.containsValue(arena)) || (pl.player2.containsKey(p.getName()) && pl.player1.containsValue(arena))) {
                        RemovePlayerMethod.removePlayer(p);
                        tpToLobbyMethod.tpToLobby(p, arena);
                        s.sendMessage(pl.prefix + "You have left the arena.");
                        RemovePlayerMethod.removePlayer(p2);
                        tpToLobbyMethod.tpToLobby(p2, arena);
                        p2.sendMessage(pl.prefix + "Your opponent �3" + p.getDisplayName() + " �6has left the game!");
                    }

                    if (pl.player1.containsKey(p.getName())) {
                        if (!pl.player2.containsValue(arena)) {
                            RemovePlayerMethod.removePlayer(p);
                            tpToLobbyMethod.tpToLobby(p, arena);
                            s.sendMessage(pl.prefix + "You have left the arena.");
                        }
                    } else if (pl.player2.containsKey(p.getName())) {
                        if (!pl.player1.containsValue(arena)) {
                            RemovePlayerMethod.removePlayer(p);
                            tpToLobbyMethod.tpToLobby(p, arena);
                            s.sendMessage(pl.prefix + "You have left the arena.");
                        }
                    } else {
                        s.sendMessage(pl.prefix + "You are not in a game!");
                    }
                } else {
                    s.sendMessage(pl.prefix + pl.noPermission);
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect usage (/bx leave)");
            }
        } else {
            s.sendMessage(ChatColor.RED + "You must be a player to use this command!");
        }
    }
}
