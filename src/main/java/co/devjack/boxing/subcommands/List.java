package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class List {
    private BoxingMain pl;

    public List(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public List(CommandSender s, Command c, String[] a) {
        if (s.hasPermission("bx.player")) {
            s.sendMessage(pl.prefix + "All boxing arenas:");
            for (String arena : pl.DataConfig.getConfigurationSection("Arenas").getKeys(false)) {
                if (arena != null) {
                    s.sendMessage("�6- " + arena);
                }
            }
        }
    }
}
