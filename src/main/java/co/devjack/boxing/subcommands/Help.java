package co.devjack.boxing.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Help {
    public Help(CommandSender s, Command c, String[] a) {
        if (s.hasPermission("bx.player") || s.hasPermission("bx.admin")) {
            s.sendMessage("�3---> �6Boxing �3<---");
            s.sendMessage("�6 Plugin made by: �3crushh87");
            s.sendMessage("�3/bx help �6- Shows this page");
            s.sendMessage("�3/bx join <arena> �6- Joins an arena");
            s.sendMessage("�3/bx leave �6- Leaves an arena");
            s.sendMessage("�3/bx list �6- Lists all arenas.");
        }

        if (s.hasPermission("bx.admin")) {
            s.sendMessage("�6Admin Help -");
            s.sendMessage("�3/bx createarena <name> �6- Creates a new arena ");
            s.sendMessage("�3/bx delarena <name> �6- Deletes an arena");
            s.sendMessage("�3/bx setspawn1 <arena> �6- Sets the first spawn for an arena");
            s.sendMessage("�3/bx setspawn2 <arena> �6- Sets the second spawn for an arena");
        }
    }
}
