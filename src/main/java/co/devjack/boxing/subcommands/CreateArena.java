package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CreateArena {
    private BoxingMain pl;

    public CreateArena(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public CreateArena(CommandSender s, Command c, String[] a) {
        if (s.hasPermission("bx.admin")) {
            if (a.length == 2) {
                String map = a[1].toLowerCase();
                if (!pl.DataConfig.contains("Arenas." + map)) {
                    pl.DataConfig.set("Arenas." + map, "");
                    s.sendMessage(pl.prefix + "Arena " + map + " �6created!");
                } else {
                    s.sendMessage(pl.prefix + "Arena already exists");
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect Usage. /bx createarena <Name>");
            }
        } else {
            s.sendMessage(pl.prefix + pl.noPermission);
        }
    }
}
