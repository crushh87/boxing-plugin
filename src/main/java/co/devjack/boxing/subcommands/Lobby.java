package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Lobby {
    private BoxingMain pl;

    public Lobby(BoxingMain instance) {
        pl = instance;
    }

    @SuppressWarnings("static-access")
    public Lobby(CommandSender s, Command c, String[] a) {
        if (s instanceof Player) {
            if (a.length == 2) {
                if (s.hasPermission("bx.admin")) {
                    String map = a[1].toLowerCase();
                    if (pl.DataConfig.contains("Arenas." + map)) {
                        Location lobby = ((Player) s).getLocation();
                        pl.DataConfig.set("Arenas." + map + ".Lobby.x", lobby.getX());
                        pl.DataConfig.set("Arenas." + map + ".Lobby.y", lobby.getY());
                        pl.DataConfig.set("Arenas." + map + ".Lobby.z", lobby.getZ());
                        pl.DataConfig.set("Arenas." + map + ".Lobby.pitch", lobby.getPitch());
                        pl.DataConfig.set("Arenas." + map + ".Lobby.yaw", lobby.getYaw());
                        pl.DataConfig.set("Arenas." + map + ".Lobby.world", lobby.getWorld().getName());
                        s.sendMessage(pl.prefix + "Lobby saved on arena �3" + map);
                    } else {
                        s.sendMessage(pl.prefix + "Arena does not exist!");
                    }
                } else {
                    s.sendMessage(pl.prefix + pl.noPermission);
                }
            } else {
                s.sendMessage(pl.prefix + "Incorrect Usage (/bx setlobby <Arena>");
            }
        } else {
            s.sendMessage(pl.prefix + "You have to be a player to use this command!");
        }
    }
}
