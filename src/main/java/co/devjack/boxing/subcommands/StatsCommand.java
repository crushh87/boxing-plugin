package co.devjack.boxing.subcommands;

import co.devjack.boxing.BoxingMain;
import co.devjack.boxing.stats.MyStatsMethod;
import co.devjack.boxing.stats.TopLosersMethod;
import co.devjack.boxing.stats.TopWLR;
import co.devjack.boxing.stats.TopWinsMethod;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatsCommand {
    private static BoxingMain pl;

    public StatsCommand(BoxingMain instance) {
        pl = instance;

    }

    @SuppressWarnings("static-access")
    public StatsCommand(CommandSender s, Command c, String[] a) {
        Player p = (Player) s;

        if (p.hasPermission("bx.player")) {
            if (a.length == 1) {
                p.sendMessage("�6---> �3Boxing Stats! �6<---");
                p.sendMessage("�3/bx stats mystats �6 - Shows your stats ");
                p.sendMessage("�3/bx stats topwins �6 - Shows the players with the most wins");
                p.sendMessage("�3/bx stats toplose �6 - Shows the players with the most losses");
                p.sendMessage("�3/bx stats topwlr �6 -  Shows the players with the top Win/Loss Ratios");
            } else if (a[1].equalsIgnoreCase("mystats")) {
                MyStatsMethod.showMyStats(p);
            } else if (a[1].equalsIgnoreCase("topwins")) {
                TopWinsMethod.showTopWins(p);
            } else if (a[1].equalsIgnoreCase("toplose")) {
                TopLosersMethod.showTopLosers(p);
            } else if (a[1].equalsIgnoreCase("topwlr")) {
                TopWLR.showTopWLR(p);
            }
        } else {
            p.sendMessage(pl.prefix + pl.noPermission);
        }
    }
}
